import Vue from 'nativescript-vue'
import App from './components/App'
import Axios from 'axios'
Axios.defaults.baseURL = "https://son.dunarr.com/api/v1";
// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production')

Vue.registerElement(
  'RadSideDrawer',
  () => require('nativescript-ui-sidedrawer').RadSideDrawer
)

new Vue({

  render: h => h('frame', [h(App)])
}).$start()
